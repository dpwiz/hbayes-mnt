{- | A comparison of influence diagram solution with references

-}
module Bayes.Test.InfluencePatterns(
   testStudentDecisions
 ) where

import Test.HUnit.Base(assertBool)

import Bayes.Examples.Influence
import Bayes.InfluenceDiagram

testStudentDecisions ::  IO ()
testStudentDecisions = do
  let result1 = solveInfluenceDiagram student
      (e1,pr,s) = studentDecisionVars
      l1 = map decisionToInstantiation result1
  assertBool "Student Network" $ l1 == [[[e1 =: (0::Int)]],[[s =: (0::Int),pr =: False],[s =: (0::Int),pr =: True]]]
  let result2 = solveInfluenceDiagram studentSimple
      e2 = studentSimpleDecisionVar
      l2 = map decisionToInstantiation result2
  assertBool "Simple Student Network" $ l2 == [[[ e2 =: (1 :: Int)]]]
