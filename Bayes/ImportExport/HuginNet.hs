-- | Parser for a subset of the Hugin Net language
module Bayes.ImportExport.HuginNet(
    importBayesianGraph
    ) where

import Text.ParserCombinators.Parsec.Prim
import Text.ParserCombinators.Parsec.Char
import Text.ParserCombinators.Parsec.Combinator

import Data.Maybe(mapMaybe,fromJust)
import Bayes.ImportExport.HuginNet.Splitting
import qualified Data.Map as Map
import Bayes.Factor
import Bayes
import Bayes.Factor.CPT(changeVariableOrder)
import Bayes.Network (MaybeNode(..))
import Bayes.BayesianNetwork

--import Debug.Trace

--debug a = trace (show a) a

data Section = Net
             | Node String [String] Int
             | Potential [String] [String]
             deriving(Eq,Show)

name :: Parser String
name = many1 (alphaNum <|> oneOf "_-")

sectionContent :: Parser ()
sectionContent = do
    _ <- string "{"
    _ <- newline
    _ <- many1 (noneOf "}")
    _ <- string "}"
    optional newline
    return ()

net :: Parser Section
net = do
    _ <- string "net"
    _ <- newline
    sectionContent
    return Net

levelName :: Parser [Char]
levelName = do
    _ <- char '"'
    s <- many1 (noneOf "\"")
    _ <- char '"'
    return s

-- | Node states
state :: Parser [String]
state = do
    spaces
    _ <- string "states"
    spaces
    _ <- char '='
    spaces
    _ <- char '('
    spaces
    levels <- sepEndBy1 levelName (many1 space)
    _ <- char ')'
    spaces
    _ <- char ';'
    spaces
    optional newline
    return levels

factorValues :: Parser String
factorValues = do
    spaces
    _ <- string "data"
    spaces
    _ <- char '='
    spaces
    r <- many1 (noneOf ";")
    spaces
    optional newline
    return r

unknownCommand :: Parser (Maybe a)
unknownCommand = do
    _ <- manyTill (noneOf "}") newline
    return Nothing

recognizedCommand :: Parser a -> Parser (Maybe a)
recognizedCommand c =  choice [try c >>= return . Just, unknownCommand]

node :: Parser Section
node = do
    _ <- string "node"
    spaces
    n <- name
    _ <- newline
    _ <- string "{"
    _ <- newline
    l <- many (recognizedCommand state)
    _ <- string "}"
    optional newline
    let r = concat . mapMaybe id $ l
    return $ Node n r (length r)

potential :: Parser Section
potential = do
    _ <- string "potential"
    spaces
    conditions <- manyTill anyChar newline
    _ <- string "{"
    _ <- newline
    l <- many (recognizedCommand factorValues)
    _ <- string "}"
    optional newline
    let r = concat . mapMaybe id $ l
    return $ Potential (splitCPT conditions) (splitValues r)

section :: Parser Section
section = choice [try net,try node,try potential]

comment :: Parser ()
comment = do
    _ <- string "%%"
    _ <- manyTill anyChar newline
    return ()

manyEmpty :: Parser ()
manyEmpty = skipMany (space <|> newline)

netParser :: Parser [Section]
netParser = do
    _ <- many comment
    manyEmpty
    sepEndBy1 section manyEmpty

_addVertexName :: Section
                  -> (Int, Map.Map String DV) -> (Int, Map.Map String DV)
_addVertexName (Node s _ d) (c,m) = (c+1,Map.insert s (DV (Vertex c) d) m)
_addVertexName _ (c,m) = (c,m)

addSection :: (DirectedGraph g, Factor f) =>
              Map.Map String DV
              -> Section -> GraphMonad g () (MaybeNode f) ()
addSection _m (Node _ _ _) = return ()
addSection _m Net = return ()
addSection m (Potential conditions values) = do
    let dvs = fromJust . mapM (flip Map.lookup m) $ conditions
        dst = head dvs
        conds = tail dvs
        oldOrder = conds ++ [dst]
        dvalues = map read values :: [Double]
        newvalues = changeVariableOrder (DVSet oldOrder) (DVSet dvs) dvalues
    cpt dst conds ~~ newvalues
    return ()

addVariables :: NamedGraph g =>
                Section -> GraphMonad g e (MaybeNode f) (Maybe (String, DV))
addVariables (Node s _ d) = do
    v <- variableWithSize s d
    return $ Just (s,v)
addVariables _ = return Nothing

-- | Import a bayesian network form a Hugin file.
-- Only a subset of the file format is supported.
-- You may have to convert the line endings to be able to parse a file
-- When it is succeeding, it is returing a bayesian network monad and
-- a mapping from node names to discrete variables.
importBayesianGraph :: Factor f
                    => String
                    -> IO (Maybe (BNMonad DirectedSG f (Map.Map String DV)))
importBayesianGraph s = do
    r <- readBayesianNetwork s
    case r of
        Nothing -> return Nothing
        Just s' -> return . Just $ createBayesianGraph s'

mapMaybeM :: Monad m => (a -> m (Maybe b)) -> [a] -> m [b]
mapMaybeM f l = mapM f l >>= return . mapMaybe id

createBayesianGraph :: Factor f => [Section] ->  BNMonad DirectedSG f (Map.Map String DV)
createBayesianGraph s = do
    vars <- mapMaybeM addVariables s
    let m = Map.fromList vars
    mapM_ (addSection m) s
    return m

-- | Horrible way to remove the comments
filterComment :: Bool -> String -> String
filterComment False ('%':l) = filterComment True l
filterComment False (a:l) = a:filterComment False l
filterComment False [] = []
filterComment True ('\n':l) = '\n':filterComment False l
filterComment True (_a:l) = filterComment True l
filterComment True [] = []

readBayesianNetwork :: FilePath -> IO (Maybe [Section])
readBayesianNetwork s = do
    f <- readFile s
    let result = runParser netParser () s (filterComment False f)
    case result of
        Left err -> do
            print err
            return Nothing
        Right a -> return (Just a)
