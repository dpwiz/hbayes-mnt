module Bayes.ImportExport.HuginNet.Splitting (
    splitCPT
  , splitValues
  ) where

import Data.List.Split (dropBlanks, dropDelims, oneOf, split)

splitCPT :: [Char] -> [[Char]]
splitCPT = split (dropBlanks . dropDelims $ oneOf "() |")

splitValues :: [Char] -> [[Char]]
splitValues = split (dropBlanks . dropDelims $ oneOf "() \n\t")
